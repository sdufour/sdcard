
#include <stdint.h>

#include "fifo.h"

#define END 0x1AEF7BC1
static void fail (const char * str);

typedef struct
{
    int id;
    char data[25];
    int end;
}TestObject_t;

static uint8_t buffer[sizeof(TestObject_t)*8];

int main (void)
{
    int i;
    //
    //  Build test objects
    //
    static TestObject_t o1 =
    {
        .id = 1,
        .end = END
    };

    static TestObject_t o2 =
    {
        .id = 2,
        .end = END
    };

    static TestObject_t o3 =
    {
        .id = 3,
        .end = END
    };

    static TestObject_t o4 =
    {
        .id = 4,
        .end = END
    };



    //
    //  Initialize the buffer
    //
    Queue_t queue;
    QueueStatus_t res;

    //
    //  Test a null pointer
    //
    res = Queue_init(&queue, 0, sizeof(TestObject_t), 8);

    if(res != QUEUE_FAIL)
        fail("Null pointer was given but the queue creation passed anyway");

    //
    //  Test a proper queue initialisation
    //
    res = Queue_init(&queue, buffer, sizeof(TestObject_t), 8);

    if(res != QUEUE_OK)
        fail("The queue creation failed when the given parameters were good");

    //
    //  Make sure the queue is empty
    //
    if(Queue_isEmpty(&queue) != true)
        fail("The newly created queue is not empty...");

    //
    //  Try to add an element into the queue
    //
    res = Queue_put(&queue, (uint8_t*)&o1);

    if(res != QUEUE_OK)
        fail("Failed to add an item to the queue when it should not have been a problem");

    //
    //  Make sure the element has been added and is accessible
    //
    TestObject_t * object_p;
    int n;

    res = Queue_get(&queue, (uint8_t**)&object_p);

    if(res != QUEUE_OK)
        fail("Failed to get object when the queue was not empty");

    if(object_p->id != 1)
        fail("The object returned is not the same object that was added.");

    if(object_p->end != END)
        fail("The end of the object is invalid!");

    Queue_getBlock(&queue, (uint8_t**)&object_p, &n);

    if(n != 1)
        fail("Block size is too big");

    //
    //  Make sure we can add an element and the same one as before is still pointed
    //
    res = Queue_put(&queue, (uint8_t*)&o2);

    if(res != QUEUE_OK)
        fail("Failed to add a second item to the queue");

    res = Queue_get(&queue, (uint8_t**)&object_p);

    Queue_getBlock(&queue, (uint8_t**)&object_p, &n);

    if(n != 2)
        fail("Block size is too big");

    if(res != QUEUE_OK)
        fail("Failed to get object when the queue was not empty");

    if(Queue_isEmpty(&queue))
        fail("Queue is said to be empty when it is not....");

    if(Queue_isFull(&queue))
        fail("Queue is said to be full when it is not....");

    if(object_p->id != 1)
        fail("The object returned is not the same object that was added.");

    if(object_p->end != END)
        fail("The end of the object is invalid!");


    //
    //  Test the buffer full return value
    //
    for(i = 0 ; i < 6 ; i++)
    {
        res = Queue_put(&queue, (uint8_t*)&o3);

        if(res != QUEUE_OK)
            fail("We cannot add the number of elements we want to the queue");
    }

    Queue_getBlock(&queue, (uint8_t**)&object_p, &n);

    if(n != 8)
        fail("Block size is too big");

    //
    //  Make sure we are told that the buffer is full
    //
    if(Queue_isFull(&queue) != true)
        fail("The queue is full but we are being lied to!");

    if(Queue_getLength(&queue) != 8)
        fail("We have 8 elements in the queue but that's not what it's telling us");

    if(Queue_isEmpty(&queue))
        fail("Queue is said to be empty when it is full....");


    //
    //  Make sure when we add an element we are told that the queue is full
    //
    res = Queue_put(&queue, (uint8_t*)&o4);

    if(res != QUEUE_FULL)
        fail("The queue is full but we seem to be able to add stuff anyway... not very cool...");

    //
    //  Make sure we can still liberate an element
    //
    res = Queue_remove(&queue);

    if(res != QUEUE_OK)
        fail("The queue is full and we can't remove an element?");

    Queue_getBlock(&queue, (uint8_t**)&object_p, &n);

    if(n != 7)
        fail("Block size is incorrect");

    res = Queue_put(&queue, (uint8_t*)&o4);

    if(res != QUEUE_OK)
        fail("We don't seem to be able to add an element to the queue");

    Queue_getBlock(&queue, (uint8_t**)&object_p, &n);

    if(n != 7)
        fail("Block size is incorrect");

    //
    //  Make sure that after all this we are refering to object 3.
    //
    res = Queue_get(&queue, (uint8_t**)&object_p);

    if(res != QUEUE_OK)
        fail("Failed to get the first object in the queue...");

    if(object_p->id != 2)
        fail("We are not refering to the correct object...");

    for(i = 0 ; i < 7 ; i++)
    {
        res = Queue_remove(&queue);

        if(res != QUEUE_OK)
            fail("Failed to empty out queue");
    }

    res = Queue_get(&queue, (uint8_t**)&object_p);

    if(res != QUEUE_OK)
        fail("Failed to get an object from the queue when there should be an object remaining");

    if(object_p->id != 4)
        fail("Invalid object in the first position of the queue");

    if(object_p->end != END)
        fail("Invalid end data");

    Queue_getBlock(&queue, (uint8_t**)&object_p, &n);

    if(n != 1)
        fail("Block size is incorrect");

    //
    //  Queue size should be of one
    //
    if(Queue_getLength(&queue) != 1)
        fail("Queue is of incorrect length");

    if(Queue_isEmpty(&queue))
        fail("Queue should not be empty");

    //
    //  Make sure we can get back to a size of 1
    //
    res = Queue_remove(&queue);

    if(res != QUEUE_OK)
        fail("Failed to empty out queue");

    if(Queue_getLength(&queue) != 0)
        fail("Invalid queue length");

    if(Queue_isEmpty(&queue) != true)
        fail("Queue is not empty when it should be.");

    if(Queue_isFull(&queue))
        fail("Queue should not be full");

    Queue_getBlock(&queue, (uint8_t**)&object_p, &n);

    if(n != 0)
        fail("Block size is incorrect");

    //
    //  Test another overflow
    //
    Queue_put(&queue, (uint8_t*)&o1);
    Queue_put(&queue, (uint8_t*)&o2);
    Queue_put(&queue, (uint8_t*)&o3);
    Queue_put(&queue, (uint8_t*)&o4);

    Queue_get(&queue, (uint8_t**)&object_p);

    if(object_p->id != 1)
        fail("Invalid query");

    if(Queue_getLength(&queue) != 4)
        fail("Invalid queue length");

    Queue_removeBlock(&queue, 3);

    Queue_get(&queue, (uint8_t**)&object_p);

    if(object_p->id != 4)
        fail("Invalid query");

    if(Queue_getLength(&queue) != 1)
        fail("Invalid queue length");

    Queue_put(&queue, (uint8_t*)&o1);
    Queue_put(&queue, (uint8_t*)&o2);
    Queue_put(&queue, (uint8_t*)&o3);
    Queue_put(&queue, (uint8_t*)&o4);

    Queue_remove(&queue);

    Queue_get(&queue, (uint8_t**)&object_p);

    if(object_p->id != 1)
        fail("Again, invalid object");

    for(i = 0 ; i < 4 ; i++)
        Queue_remove(&queue);

    if(Queue_getLength(&queue) != 0 || Queue_isEmpty(&queue) != true)
        fail("The empty queue is lying...");


    //
    //  If we get here, all seems good
    //
    //printf("Test successfully passed");


    while(1);
}


static void fail (const char * str)
{
    //printf(str);
    //printf("\n");
    //exit(0);
	while(1);
}
