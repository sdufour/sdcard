#ifndef __SD_DRIVER_EXTENSION__H__
#define	__SD_DRIVER_EXTENSION__H__

#include "stm32f7xx.h"
#include "stm32f7xx_hal_def.h"
#include "stm32f7xx_hal_sd.h"

HAL_SD_ErrorTypedef HAL_SD_CheckWriteOperation_part1(SD_HandleTypeDef *hsd, uint32_t Timeout);
HAL_SD_ErrorTypedef HAL_SD_CheckWriteOperation_part2(SD_HandleTypeDef *hsd, uint32_t Timeout);
HAL_SD_ErrorTypedef HAL_SD_StopTransfer_NO_WAIT(SD_HandleTypeDef *hsd);


#endif
