//
//  Self Inclusion
//
#include "fifo.h"

//
//  Compiler level libs
//
#include <stddef.h>
#include <string.h>


//#define     MIN(A,B)    ((A) < (B) ? (A) : (B))
static unsigned int MIN( unsigned int A, unsigned int B)
{
	return ((A) < (B) ? (A) : (B));
}

/*!
 *  \brief  This function initialises a queue.
 *
 *  \param  queue [in] : This is a queue structure. All parameters will be initialized via this function
 *  \param  buffer [in] : This is the buffer that will be used to hold data in the queue. The size of this
 *                          buffer should be of elementSize*numberOfElements bytes.
 *  \param  elementSize [in] : This is the size of a single element to be contained in the queue, in bytes.
 *  \param  numberOfElements [in] : This is the number of elements that should be contained in the queue.
 *
 *  \return [out] : QUEUE_OK if the initialisation was completed adequately
 *                  QUEUE_FAIL if the numberOfElements is not a multiple of elementSize.
 */
QueueStatus_t Queue_init (Queue_t * queue, void * buffer, int elementSize, int numberOfElements)
{
    //
    //  Copy given data into the queue structure
    //
    queue->length = numberOfElements;
    queue->buffer = buffer;
    queue->elementSize = elementSize;

    //
    //  Initialize the data
    //
    queue->in = 0;
    queue->out = 0;
    queue->isEmpty = true;
    queue->isFull = false;

    //
    //  Make sure the buffer isn't null
    //
    if(buffer == NULL)
        return QUEUE_FAIL;

    //
    //  Indicate a successful initialisation
    //
    return QUEUE_OK;

}



QueueStatus_t Queue_put (Queue_t * queue, void * object)
{
    //
    //  If the queue is full, stop here.
    //
    if(queue->isFull)
        return QUEUE_FULL;

    //
    //  Otherwise, find the spot the object should be copied to
    //
    void * p = queue->buffer + (queue->in++  * queue->elementSize);

    //
    //  And copy the object in.
    //
    memcpy(p, object, queue->elementSize);

    //
    //  Wrap the in pointer
    //
    queue->in %= queue->length;

    //
    //  Indicate we are no longer empty
    //
    queue->isEmpty = false;

    //
    //  And check if we are full
    //
    if(queue->in == queue->out)
        queue->isFull = true;

    //
    //  Indicate that the operation was successful
    //
    return QUEUE_OK;

}


QueueStatus_t Queue_get (Queue_t * queue, void ** object_p)
{
    //
    //  If the queue is empty, return immediately
    //
    if(queue->isEmpty)
        return QUEUE_EMPTY;

    //
    //  Find the point we are supposed to be at
    //
    void * p = queue->buffer + (queue->out * queue->elementSize);

    //
    //  indicate this point to the outside world
    //
    *object_p = p;

    //
    //  Indicate that we successfully fetched the data
    //
    return QUEUE_OK;


}

QueueStatus_t Queue_getBlock (Queue_t * queue, void ** object_p, int * blockSize)
{
    //
    //  Init block size to a safe state
    //
    *blockSize = 0;

    //
    //  Get the first object as we would normally
    //
    QueueStatus_t status = Queue_get(queue, object_p);

    //
    //  If this operation failed, fail in the same manner
    //
    if(status != QUEUE_OK)
        return status;

    //
    //  Figure out how many elements are contiguous
    //
    *blockSize = MIN(Queue_getLength(queue), (unsigned int)(queue->length - queue->out));

	return QUEUE_OK;
}

QueueStatus_t Queue_remove (Queue_t * queue)
{
    //
    //  If the queue is empty, return immediately
    //
    if(queue->isEmpty)
        return QUEUE_EMPTY;

    //
    //  Increment the out pointer to remove an object
    //
    queue->out++;

    //
    //  Wrap the out pointer
    //
    queue->out %= queue->length;

    //
    //  Show that we are no longer full
    //
    queue->isFull = false;

    //
    //  Check if we are empty
    //
    if(queue->in == queue->out)
        queue->isEmpty = true;

    //
    //  Indicate that we successfully fetched the data
    //
    return QUEUE_OK;
}


QueueStatus_t Queue_removeBlock (Queue_t * queue, int blockSize)
{

    int i;
    QueueStatus_t status;


    for(i = 0 ; i < blockSize ; i++)
    {
        status = Queue_remove(queue);

        if(status != QUEUE_OK)
            return status;
    }

    return QUEUE_OK;

}

bool Queue_isEmpty (Queue_t * queue)
{
    return queue->isEmpty;
}



bool Queue_isFull (Queue_t * queue)
{
    return queue->isFull;
}


int Queue_getLength (Queue_t * queue)
{
    //
    //  If the queue is full, we will get a length of 0.
    //  This is corrected by the following condition statement
    //
    if(queue->isFull)
        return queue->length;


    return (unsigned int)(queue->in - queue->out);
}






