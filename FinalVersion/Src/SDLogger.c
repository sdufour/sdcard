
#include "stm32f7xx_hal.h"
#include "ff.h"
#include "ff_gen_drv.h"
#include "sd_diskio.h" 

#include <stdlib.h> 
#include <string.h>
#include <stddef.h>
#include "fifo.h"

#include "SDLogger.h"

static char SD_Path[4];  /* SD logical drive path */


static FATFS SDFatFs; /* File system object for SD card logical drive */
#define SZ_TBL 100
static DWORD clmt[SZ_TBL]; //Table for storing the allocation table for the file
static FIL MyFile;     /* File object */

//variable to remember if the file was initialized
static bool initialized = false;

//Buffers for double buffering
static Queue_t fifo;
static const int elementSize = 512;
static const int numberOfElements = 16;
static uint8_t buffer[elementSize*numberOfElements*2] __attribute__((aligned (4)));


volatile static bool transferComplete = true;
static uint32_t byteswritten;
static FRESULT res;

//Private functions
static bool __WriteData(const uint8_t buffer [4096]);


bool SDLogger_Init( bool format, const char* filename, int size)
{
	 FRESULT res;
	
	//Please close the file before trying to open another one...
	if(initialized)	{ while(1){};}
	
	
	/* FatFS: Link the SD disk I/O driver */
	if(FATFS_LinkDriver(&SD_Driver, SD_Path)!=0)
		return false;

	/* Register the file system object to the FatFs module */
	if(f_mount(&SDFatFs, (TCHAR const*)SD_Path, 0) != FR_OK){
		/* FatFs Initialization Error */
		return false;
	}
	
	if(format)
	{
		/* Create a FAT file system (format) on the logical drive */
		/* WARNING: Formatting the uSD card will delete all content on the device */
		if(f_mkfs((TCHAR const*)SD_Path, 0, 0) != FR_OK){
			/* FatFs Format Error*/
			return false;
		}
	}
	else
	{
		//nothing to do! :)
	}
	
	//Test If Filename TooLong
	int filenameSize = strlen(filename);
	if(filenameSize>=20)
	{
		return false;
	}
	
	char filenameToUse[30];
	int i = 0;
	sprintf ( filenameToUse,"log%d.txt",i );
	
	//Test if file exist, if it does, add a number to the end...
	while(f_stat(filenameToUse, NULL ) != FR_NO_FILE)
	{
		sprintf ( filenameToUse,"log%d.txt",++i );
		if(i>100)
		{
			return false;
		}
	}
	
	// Code for creating a directory with increasing number
	/*
	DIR dp;
	FILINFO fno;
	int LastFolderNumber = 0;
	char DataDir[100];
	uint8_t rtext[100];
	char FilePath[100];
	char FilePathBin[100];
	res = f_findfirst(&dp, &fno, "", "DATA*");
	while (res == FR_OK && fno.fname[0]) 
	{
		LastFolderNumber = atoi(&fno.fname[4]);
		res = f_findnext(&dp, &fno);
	}
	f_closedir(&dp);
	
	strcpy(DataDir, "DATA");
			sprintf((char *)rtext, "%d", LastFolderNumber+1);
			strcat(DataDir, (char *)rtext); 
			res	= f_mkdir(DataDir);
			if(res == FR_OK || res == FR_EXIST)
			{
				strcpy(FilePath, DataDir);
				strcat(FilePath, "/STM3222.txt");
				strcpy(FilePathBin, DataDir);
				strcat(FilePathBin, "/data.bin");
	*/
	
	/* Create & Open a new text file object with write access */
	if( f_open(&MyFile, filenameToUse, FA_CREATE_ALWAYS | FA_WRITE) !=	FR_OK)
	{
		return false;
	}
	
	//TODO: space on sdCard for file size...
	/* Allocate the full size of the file */
	res = f_lseek(&MyFile, size);           /* Expand file size (cluster pre-allocation) */
	if (res || f_tell(&MyFile) != size) /* Check if the file has been expanded */
	{
		return false;
	}

	/* Return to the begening of the file */
	res = f_lseek(&MyFile, 0);
	if (res || f_tell(&MyFile) != 0) /* Check if the pointer is at begening */
	{
		return false;
	}
	
	/* Activate FastSeek */
	// This will put the allocation table for the file in ram
		MyFile.cltbl = clmt; /* Enable fast seek feature (cltbl != NULL) */
    	clmt[0] = SZ_TBL;                      /* Set table size */
    	res = f_lseek(&MyFile, CREATE_LINKMAP);     /* Create CLMT */
		
	
	/* setup queue*/
	if( Queue_init (&fifo, buffer, elementSize, numberOfElements) != QUEUE_OK)
		return false;
	
	
	//We are finished!
	initialized = true;
	return true;
}

enum LoggerState { LoggerState_WriteBegin, LoggerState_WriteEnd, LoggerState_STALL1, LoggerState_STALL2, LoggerState_CheckStatus  };
int writeStep = 0;

bool SDLogger_Store( const uint8_t newData[512] )
{
	static int packetToWrite = 0;
	
	//static uint64_t timeOfLastStep = 0;
	bool result = true; 
	
	if(!initialized)
		return false;

	if(newData)
	{
		Queue_put (&fifo, (void*)newData);
	}
	
	//If less than 8 packets, continue waiting before writing the packet.
	uint8_t * dataToWrite;
	int blockSize;
	if(writeStep == LoggerState_WriteBegin)
	{
		QueueStatus_t status = Queue_getBlock (&fifo, (void**)(&dataToWrite), &blockSize);
		if (status  == QUEUE_EMPTY)
			return true;
		
		if(blockSize<8)
			return true;
	}
	
	//TODO remove when using larger block size
	blockSize = 8;
	setSDBlocking(false);
	
	// Do action on sd card if required
	switch( writeStep )
	{
		case LoggerState_WriteBegin:
			//Use DMA for transfert
			
			result = __WriteData(dataToWrite);
			//set the next step
			writeStep = LoggerState_WriteEnd;
			break;
		
		case LoggerState_WriteEnd:
			if(!transferComplete)
			{
				result =  true;
				break;
			}
			//DMA transfert complete
			//We can now release the buffer
			//TODO use larger block size...
			Queue_removeBlock(&fifo, 8);
			BSP_SD_GetTransferStatus(1);
			writeStep = LoggerState_STALL1;
			break;
			
		case LoggerState_STALL1:
			writeStep = LoggerState_STALL2;
			break;
		
		case LoggerState_STALL2:
			writeStep = LoggerState_CheckStatus;
			break;
			
		case LoggerState_CheckStatus:
			//todo delay here
			BSP_SD_GetTransferStatus(2);
			if((byteswritten == 0) || (res != FR_OK))
				{
				result =  false;
				break;
			}
			
			writeStep = LoggerState_WriteBegin;
			break;
	}
	
	
	setSDBlocking(true);
	
	// If any error occurs, stop writing to the sd card by marking it as non initialized
	initialized = result;
	return result;
}

bool SDLogger_Close( )
{
	if(!initialized)
			return false;
	
	//wait for no more thing to write
	while(writeStep != LoggerState_WriteBegin)
	{
		SDLogger_Store( NULL );
		HAL_Delay(2);
	}
	
			
	f_truncate(&MyFile);
	f_close(&MyFile);
	FATFS_UnLinkDriver(SD_Path);
	
	initialized = false;
	return true;
}

static bool __WriteData(const uint8_t buffer [4096])
{
	res = f_write(&MyFile, buffer, 4096, &byteswritten);
	if((byteswritten == 0) || (res != FR_OK))
	{
		return false;
	}
	return true;
}

void HAL_SD_XferCpltCallback(SD_HandleTypeDef *hsd)
{
	transferComplete = true;
}

void HAL_SD_DMA_TxCpltCallback(DMA_HandleTypeDef *hdma)
{
	
}

