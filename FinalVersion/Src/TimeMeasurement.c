#include <stdint.h>
#include <stdbool.h>
#include "stm32f7xx_hal.h"
#include "core_cm7.h"

#include "timeMeasurement.h"


#define ticsFor1us 168 
#define ticsFor1ms ( ticsFor1us * 1000 )

int64_t getTimeUs(void)
{
	uint32_t major0;
	uint32_t minor;
	uint32_t major1;
	do {
		major0 = HAL_GetTick();
		minor = SysTick->VAL;
		major1 = HAL_GetTick();
	}
	while (major0 != major1 || (minor < ticsFor1us) || (minor > (ticsFor1ms-ticsFor1us)));
	return ((int64_t)(major0+1)*1000) - ((int64_t)(minor/ticsFor1us));
}
