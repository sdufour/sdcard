
#include "SD_Driver_Extension.h"



#define SDMMC_STATIC_FLAGS               ((uint32_t)(SDMMC_FLAG_CCRCFAIL | SDMMC_FLAG_DCRCFAIL | SDMMC_FLAG_CTIMEOUT |\
                                                    SDMMC_FLAG_DTIMEOUT | SDMMC_FLAG_TXUNDERR | SDMMC_FLAG_RXOVERR  |\
                                                    SDMMC_FLAG_CMDREND  | SDMMC_FLAG_CMDSENT  | SDMMC_FLAG_DATAEND  |\
                                                    SDMMC_FLAG_DBCKEND))  




HAL_SD_ErrorTypedef HAL_SD_CheckWriteOperation_part1(SD_HandleTypeDef *hsd, uint32_t Timeout)
{
  HAL_SD_ErrorTypedef errorstate = SD_OK;
  uint32_t timeout = Timeout;
  uint32_t tmp1, tmp2;
  HAL_SD_ErrorTypedef tmp3;

  /* Wait for DMA/SD transfer end or SD error variables to be in SD handle */
  tmp1 = hsd->DmaTransferCplt; 
  tmp2 = hsd->SdTransferCplt;
  tmp3 = (HAL_SD_ErrorTypedef)hsd->SdTransferErr;
    
  while (((tmp1 & tmp2) == 0) && (tmp3 == SD_OK) && (timeout > 0))
  {
    tmp1 = hsd->DmaTransferCplt; 
    tmp2 = hsd->SdTransferCplt;
    tmp3 = (HAL_SD_ErrorTypedef)hsd->SdTransferErr;
    timeout--;
  }
  
  timeout = Timeout;
  
  /* Wait until the Tx transfer is no longer active */
  while((__HAL_SD_SDMMC_GET_FLAG(hsd, SDMMC_FLAG_TXACT))  && (timeout > 0))
  {
    timeout--;  
  }

  /* Send stop command in multiblock write */
  if (hsd->SdOperation == SD_WRITE_MULTIPLE_BLOCK)
  {
    HAL_SD_StopTransfer(hsd);
  }
	
	return errorstate;
}

HAL_SD_ErrorTypedef HAL_SD_CheckWriteOperation_part2(SD_HandleTypeDef *hsd, uint32_t Timeout)
{
  HAL_SD_ErrorTypedef errorstate = SD_OK;
  uint32_t timeout = Timeout;
	
  if ((timeout == 0) && (errorstate == SD_OK))
  {
    errorstate = SD_DATA_TIMEOUT;
  }
  
  /* Clear all the static flags */
  __HAL_SD_SDMMC_CLEAR_FLAG(hsd, SDMMC_STATIC_FLAGS);
  
  /* Return error state */
  if (hsd->SdTransferErr != SD_OK)
  {
    return (HAL_SD_ErrorTypedef)(hsd->SdTransferErr);
  }
  
  /* Wait until write is complete */
  while(HAL_SD_GetStatus(hsd) != SD_TRANSFER_OK)
  {    
  }

  return errorstate; 
}


HAL_SD_ErrorTypedef HAL_SD_StopTransfer_NO_WAIT(SD_HandleTypeDef *hsd)
{
  SDMMC_CmdInitTypeDef sdmmc_cmdinitstructure;
  HAL_SD_ErrorTypedef errorstate = SD_OK;
  
  /* Send CMD12 STOP_TRANSMISSION  */
  sdmmc_cmdinitstructure.Argument         = 0;
  sdmmc_cmdinitstructure.CmdIndex         = SD_CMD_STOP_TRANSMISSION;
  sdmmc_cmdinitstructure.Response         = SDMMC_RESPONSE_SHORT;
  sdmmc_cmdinitstructure.WaitForInterrupt = SDMMC_WAIT_NO;
  sdmmc_cmdinitstructure.CPSM             = SDMMC_CPSM_ENABLE;
  SDMMC_SendCommand(hsd->Instance, &sdmmc_cmdinitstructure);
  
  /* Check for error conditions */
  //errorstate = SD_CmdResp1Error(hsd, SD_CMD_STOP_TRANSMISSION);
  
  return errorstate;
}

