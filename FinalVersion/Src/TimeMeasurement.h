#ifndef __TIME_MEASUREMENT__H__
#define __TIME_MEASUREMENT__H__

#include <stdint.h>

int64_t getTimeUs(void);
#endif
