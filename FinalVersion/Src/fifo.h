#ifndef __FIFO__H__
#define __FIFO__H__

#include <stdbool.h>
#include <stdint.h>


/*!
 *  \brief  This is the queue structure. Do not set or get any parameters
 *          manually. They should all be accessed through the functions
 *          listed below.
 */
typedef struct
{
    int * buffer;
    int in;
    int out;
    int elementSize;
    int length;
    bool isEmpty;
    bool isFull;
}
Queue_t;

/*!
 *  These are possible statuses returned by the Queue function
 */
typedef enum
{
    QUEUE_OK = 0,
    QUEUE_FAIL,
    QUEUE_EMPTY,
    QUEUE_FULL
}QueueStatus_t;


/*!
 *  \brief  This function initialises a queue.
 *
 *  \param  queue [in] : This is a queue structure. All parameters will be initialized via this function
 *  \param  buffer [in] : This is the buffer that will be used to hold data in the queue. The size of this
 *                          buffer should be of elementSize*numberOfElements bytes.
 *  \param  elementSize [in] : This is the size of a single element to be contained in the queue, in bytes.
 *  \param  numberOfElements [in] : This is the number of elements that should be contained in the queue.
 *
 *  \return [out] : QUEUE_OK if the init was successfully done
 *                  QUEUE_FAIL if the buffer is null. This can be used to catch malloc errors more elegantly
 */
QueueStatus_t Queue_init (Queue_t * queue, void * buffer, int elementSize, int numberOfElements);


/*!
 *  \brief  This function will place an element in the queue.
 *
 *  \param  Queue_t * queue [in/out] : The queue to be interacted with
 *  \param  void * object [in] : The object that will be COPIED into the queue.
 *
 *  \return QueueStatus_t [out] : QUEUE_OK if the operation was successful
 *                                QUEUE_FULL if the queue is full
 */
QueueStatus_t Queue_put (Queue_t * queue, void * object);

/*!
 *  \brief  This function will return a reference to the first element in the queue. It will
 *              not delete it.
 *
 *  \param  Queue_t * queue [in/out] : The queue to be interacted with.
 *  \param  void ** object [out] : The pointer which will be redirected to the first queue element
 *
 *  \return QueueStatus_t [out] : QUEUE_OK if the operation was successful.
 *                                QUEUE_EMPTY if the queue is completely empty.
 */
QueueStatus_t Queue_get (Queue_t * queue, void ** object_p);


/*!
 *  \brief  This function will return a reference to the first and indicate the number of
 *              contiguous elements following that element. This will allow the user to
 *              mass copy data out of the FIFO. This will not remove elements from the
 *              FIFO
 *
 *  \param  Queue_t * queue [in/out] : The queue to be interacted with.
 *  \param  void ** object [out] : The pointer which will be redirected to the first queue element
 *  \param  int * blockSize [out] : The number of available elements
 *
 *  \return QueueStatus_t [out] : QUEUE_OK if the operation was successful.
 *                                QUEUE_EMPTY if the queue is completely empty.
 */
QueueStatus_t Queue_getBlock (Queue_t * queue, void ** object_p, int * blockSize);


/*!
 *  \brief  This removes the first element from the queue.
 *
 *  \return QueueStatus_t : QUEUE_OK if the item was successfully removed
 *                          QUEUE_EMPTY if the queue is empty
 */
QueueStatus_t Queue_remove (Queue_t * queue);

/*!
 *  \brief This removes n elements from the queue.
 *
 *  \return QueueStatus_t : QUEUE_OK if the item was successfully removed
 *                          QUEUE_EMPTY if the queue is empty
 */
QueueStatus_t Queue_removeBlock (Queue_t * queue, int blockSize);

/*!
 *  \brief  This function will return whether the queue is empty.
 */
bool Queue_isEmpty (Queue_t * queue);


/*!
 *  \brief  This function will return whether the queue is full.
 */
bool Queue_isFull (Queue_t * queue);

/*!
 *  \brief  This function will return the number of elements in the queue
 */
int Queue_getLength (Queue_t * queue);


#endif




