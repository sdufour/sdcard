#ifndef __SD_LOGGER__H__
#define __SD_LOGGER__H__

/*!
* \brief This Function initialise everything related to the SD card logger.
*	It format the card if required
*	Create a file with the given name
*	Allocate the specified space for that file on disk
*	Put in memory the file allocation table
*	make the file object in memory ready to write data
*
* \param format : set to true to format the sd card on boot *Warning* File will get lost...
* \param filename: the name of the file to log into
* \return true if every step succeed
*/
bool SDLogger_Init( bool format, const char* filename, int size);


/*!
* \brief Log the given data to SD card
* First the data provided is stored in a internal buffer.
* Then actions are taken to write the data to the SD card in chunk
* 
* \param array The data to store. Must be a packet of 512 bytes.
* \return true if every step succeed
*/
bool SDLogger_Store( const uint8_t array[512] );

/*!
* \brief Close the file and trim the unused space from it.
* \return true if every step succeed
*/
bool SDLogger_Close( void );





#endif
